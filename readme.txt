*********Libraries***********
pip3 install django

Django
django-cors-headers
djangorestframework
mysqlclient



*********** Create django project ***********
django-admin startproject myproject


********* DB Config ******************
DATABASES = {
   'default': {
      'ENGINE': 'django.db.backends.sqlite3',
      'NAME': 'database.sql',
      'USER': '',
      'PASSWORD': '',
      'HOST': '',
      'PORT': '',
   }
}

or

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'databaseName',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': 'localhost',
        'PORT': '3306',
       
    }
}


*********** Run project *****************
python manage.py runserver

************* Add another app **********
python manage.py startapp appName

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'corsheaders',
    'appName'
]

*********** develop api for react *************

ALLOWED_HOSTS = ["*"]
CORS_ORIGIN_ALLOW_ALL = True


********* ProjectName/urls.py***********
from django.conf.urls import url, include
from django.conf import settings
from django.conf.urls.static import static

 
urlpatterns = [ 
    url(r'^', include('AppName.urls')),
]


********** AppName views.py***************

@csrf_exempt

def test(request):
	return HttpResponse("Hi", content_type="text")
	
********  appName urls.py ****************
from django.conf.urls import include, url
from performance_analysis import views

 
urlpatterns = [ 
    url(r'^test/$', views.test),
 ]
 
 
 python3 manage.py  makemigrations
 python3 manage.py  migrate
 
