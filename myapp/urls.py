from django.conf.urls import include, url
from myapp import views

 
urlpatterns = [ 
    url(r'^test/$', views.test),
    url(r'^show/$', views.get_data),
    url(r'^add/$', views.put_data),
    url(r'^raw_data/$', views.get_raw_data),
    url(r'^find/$', views.filter_data),
 ]