from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Users
import json,re,datetime
from django.db import connection
from collections import OrderedDict 
from html import escape,unescape


# Create your views here.

from decimal import Decimal
def default(o):
    if isinstance(o, (datetime.date, datetime.datetime)):
        return o.isoformat()
    if isinstance(o, (datetime.time, datetime.datetime)):
        return o.isoformat()
    if isinstance(o, Decimal):
        return str(o)

def make_query(stmt):
    cursor = connection.cursor() 
    try:
        cursor.execute(stmt)
    except:
        return "syntax error"
    det=cursor.fetchall()
    # print(det)
    header=[]
    for val in cursor.description:
        header.append(val[0])
    header_arr=[]
    mylist=[]
    c=0
    for val in det:          
        try:
            c=0
            list1=OrderedDict()
            for value in val:
                orig_val=unescape(str(value))
                if re.search("^\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}(.\d{1,6})?$",orig_val):
                    orig_val=utc_to_ist(re.search("\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}", orig_val).group(0))
                list1.update(OrderedDict({cursor.description[c][0]: orig_val}))
                c+=1
            mylist.append(list1)
        except Exception as e:
            print(str(e))  
    return str(json.dumps(mylist,sort_keys=False,indent=1,default=default))



@csrf_exempt
def test(request):
	return HttpResponse("Hi", content_type="text")


@csrf_exempt
def get_data(request):
   return HttpResponse(json.dumps([dict(item) for item in Users.objects.all().values('name','email')]),content_type="text/json")


@csrf_exempt
def filter_data(request):
    name=request.GET.get("name")
    return HttpResponse(json.dumps([dict(item) for item in Users.objects.filter(name=name).values('name','email')]),content_type="text/json")

@csrf_exempt
def get_raw_data(request):
    # stmt="select * from myapp_users;"
    # cursor = connection.cursor() 
    # try:
    #     cursor.execute(stmt)
    # except:
    #     return "syntax error"
    # det=cursor.fetchall()
    # print(det)
    return HttpResponse(make_query("select * from myapp_users;"), content_type="text")


@csrf_exempt
def put_data(request):
    try:
        print(request.GET)
        users=Users()
        users.name=request.POST.get("name")
        users.email=request.POST.get("email")
        users.save()
        if users!=None:
            return HttpResponse("success",content_type="text")
        else:
            return HttpResponse("Failed",content_type="text")     
    except Exception as e:
        print(str(e))
        return HttpResponse("Failed",content_type="text")   




