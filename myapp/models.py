from django.db import models

# Create your models here.


# Create your models here.
class Users(models.Model):
    id = models.AutoField(primary_key=True)
    name=models.CharField(max_length=32, blank=False)
    email=models.EmailField(unique=True, blank=False)
